import React from 'react';
import { Link } from 'react-router-dom';
import { getStorageItem } from '../../../utils';
import routeConfig from '../../../config/version.json';

function payPageB() {
    const curVersion = getStorageItem('meal-version');

    return (
        <div className='domain-page'>
            收银台页payment_payPageB
            <Link to='/entrance'>返回入口页</Link>
        </div>
    );
}

export default payPageB;
