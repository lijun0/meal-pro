import logo from './logo.svg';
import Routes from './routes.tsx';
import './App.css';

function App() {
  return (
    <Routes />
  );
}

export default App;
