import React from 'react';
import { Link } from 'react-router-dom';
import { getStorageItem } from '../../../utils';
import routeConfig from '../../../config/version.json';

function menuPageB() {
    const curVersion = getStorageItem('meal-version');

    return (
        <div className='domain-page'>
            点菜页poi_menuPageB
            <Link to={routeConfig[curVersion].order}>去提单</Link>
            <Link to='/entrance'>返回入口页</Link>
        </div>
    );
}

export default menuPageB;
