import React from 'react';
import { Link } from 'react-router-dom';
import { getStorageItem } from '../../../utils';
import routeConfig from '../../../config/version.json';
import './index.scss';

function menuPageA() {
    const curVersion = getStorageItem('meal-version');

    console.log(curVersion)

    return (
        <div className='domain-page'>
            点菜页poi_menuPageA
            <Link to={routeConfig[curVersion].order}>去提单</Link>
            <Link to='/entrance'>返回入口页</Link>
        </div>
    );
}

export default menuPageA;
