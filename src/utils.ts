import { parse } from '@utiljs/param';

const setStorageItem = (key, value) => {
    window.localStorage.setItem(key, value);
}

const getStorageItem = (key) => {
    const value = window.localStorage.getItem(key);
    return value;
}

const getParams: any = () => {
    const search = window.location.search;
    const queryObj = search ? parse(search) : {};
    return queryObj;
}

export { 
    getParams,
    setStorageItem,
    getStorageItem,
}