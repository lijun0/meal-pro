import React from 'react';
import { Link } from 'react-router-dom';
import { getStorageItem } from '../../../utils';
import routeConfig from '../../../config/version.json';

function previewPageB() {
    const curVersion = getStorageItem('meal-version');

    return (
        <div className='domain-page'>
            提单页order_previewPageB
            <Link to={routeConfig[curVersion].payment}>去支付</Link>
            <Link to='/entrance'>返回入口页</Link>
        </div>
    );
}

export default previewPageB;
