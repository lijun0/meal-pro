import previewPageA from './previewPageA';
import previewPageB from './previewPageB';

export {
    previewPageA,
    previewPageB,
};
