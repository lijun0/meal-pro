import React from 'react';
import { Link } from 'react-router-dom';
import routeConfig from '../../../config/version.json';
import { setStorageItem } from '../../../utils';
import './index.scss';

function PageA() {
    const renderLink = () => {
        const linkList = [];
        for (let item in routeConfig) {
            const versionItem = routeConfig[item];
            linkList.push(
                <Link
                    to={versionItem.poi}
                    key={item}
                    onClick={() => { setStorageItem('meal-version', item); }}
                >
                    进入{item}
                </Link>
            )
        }
        return linkList;
    }

    return (
        <div className='entrance-page'>
            {renderLink()}
        </div>
    );
}

export default PageA;
