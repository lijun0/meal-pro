import React, { FunctionComponent, ReactElement } from 'react';
import { RouteComponentProps } from 'react-router';
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
  RouteProps as RawRouteProps,
} from 'react-router-dom';
import { createBrowserHistory as createHistory } from 'history';
import * as AppPages from './app/pages';
import * as PoiPages from './poi/pages';
import * as OrderPages from './order/pages';
import * as PaymentPages from './payment/pages';

export const history = createHistory();

export interface RouteProps extends RawRouteProps {
  component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>,
  breadcrumbName?: string,
  exact?: boolean,
  defaultPath?: string,
  routeComponent?: FunctionComponent<RouteProps>,
}

export const routeConfig: RouteProps[] = [
  {
    path: '/entrance',
    exact: true,
    breadcrumbName: 'entrance',
    component: AppPages.pageA,
  },
//   {
//     path: '/pageB',
//     exact: true,
//     component: appPages.pageB,
//     breadcrumbName: 'pageB',
//   },
  {
    path: '/poi/menuPageA',
    exact: true,
    component: PoiPages.menuPageA,
    breadcrumbName: 'poi/menuPageA',
  },
  {
    path: '/poi/menuPageB',
    exact: true,
    component: PoiPages.menuPageB,
    breadcrumbName: 'poi/menuPageB',
  },
  {
    path: '/order/previewPageA',
    exact: true,
    component: OrderPages.previewPageA,
    breadcrumbName: 'order/previewPageA',
  },
  {
    path: '/order/previewPageB',
    exact: true,
    component: OrderPages.previewPageB,
    breadcrumbName: 'order/previewPageB',
  },
  {
    path: '/payment/payPageA',
    exact: true,
    component: PaymentPages.payPageA,
    breadcrumbName: 'payment/payPageA',
  },
  {
    path: '/payment/payPageB',
    exact: true,
    component: PaymentPages.payPageB,
    breadcrumbName: 'payment/payPageB',
  },
];


export default function Routes() {
  return (
    <Router history={history}>
      <Switch>
        {routeConfig.map(route => {
          const RouteComponent = route.routeComponent || Route;

          return <RouteComponent key={(route.path as string)} {...route} />
        })}
        <Redirect exact from="/" to="/pageA" />
        {/* <Route component={appPages.pageA} /> */}
      </Switch>
    </Router>
  );
}